#!/bin/bash

current_date=$(date +%Y\ %m\ %d)
read -p 'Please enter a file extension: ' ex
read -p "Please enter a file prefix: (Press ENTER for $current_date)" pre
file=$(find . -maxdepth 1 -name "*.$ex" | cut -d / -f2)

if [ -z "$pre" ]

then
      echo "\$pre is empty"
      current_date+=" $file"
      echo $current_date
      mv $file "$current_date"
      echo "Renaming $file to $current_date" 
else
      pre+=" $file"
      mv $file "$pre"
      echo "Renaming $file to $pre" 
fi

