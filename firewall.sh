#!/bin/bash
##alow ssh incoming connection
# spacific_ip 172.20.30.53
iptables -A INPUT -p tcp -s 172.20.30.53  --sport 513:65535 --dport 22 -m state --state NEW,ESTABLISHED -j ACCEPT
iptables -A OUTPUT -p tcp  -d 172.20.30.53 --sport 22 --dport 513:65535 -m state --state ESTABLISHED -j ACCEPT

##alow http incoming connection

iptables -A INPUT -p tcp -s 172.20.30.53  --sport 513:65535 --dport 80 -m state --state NEW,ESTABLISHED -j ACCEPT
iptables -A OUTPUT -p tcp  -d 172.20.30.53 --sport 80 --dport 513:65535 -m state --state ESTABLISHED -j ACCEPT

##alow loopback connection

iptables -A INPUT -i lo -j ACCEPT
iptables -A OUTPUT -o lo -j ACCEPT

##alow apt-get connection

iptables -A OUTPUT -m state --state RELATED,ESTABLISHED -j ACCEPT
iptables -A OUTPUT -p tcp --dport 80 -m state --state NEW -j ACCEPT
iptables -A OUTPUT -p tcp --dport 53 -m state --state NEW -j ACCEPT
iptables -A OUTPUT -p udp --dport 53 -m state --state NEW -j ACCEPT
iptables -A OUTPUT -p tcp --dport 443 -m state --state NEW -j ACCEPT
iptables -A INPUT -m state --state RELATED,ESTABLISHED -j ACCEPT


iptables -A INPUT -j DROP
iptables -A OUTPUT -j DROP
